# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class CarrierTestCase(ModuleTestCase):
    """Test Carrier configuration module"""
    module = 'carrier_configuration'

    def setUp(self):
        super(CarrierTestCase, self).setUp()


del ModuleTestCase
